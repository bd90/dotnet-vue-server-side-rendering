using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

using dotnet_vue_server_side_rendering.Models;

namespace dotnet_vue_server_side_rendering.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var initialMessages = FakeMessageStore.FakeMessages.OrderByDescending(m => m.Date).Take(2);

            var initialValues = new ClientState() {
                Messages = initialMessages,
                LastFetchedMessageDate = initialMessages.Last().Date
            };

            return View(initialValues);
        }

        [Route("/Home/fetchMessages")]
        public JsonResult FetchMessages(DateTime lastFetchedMessageDate)
        {
            return Json(FakeMessageStore.FakeMessages.OrderByDescending(m => m.Date).SkipWhile(m => m.Date >= lastFetchedMessageDate).Take(1));
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
