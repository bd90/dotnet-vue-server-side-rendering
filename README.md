# ASP.NET MVC and Vue server side rendering

## TODO

 - [X] Add Vue
 - [X] Add Microsoft.SpaService
 - [X] Webpack config
 - [X] Full server side rendering

## Setup

 - `$ npm i`
 - `$ npm i babel-core`
 - `$ npm i -g webpack`
 - `$ webpack --config webpack.client.config.js`
 - `$ webpack --config webpack.server.config.js` 